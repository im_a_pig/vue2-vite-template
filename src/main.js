import Vue from 'vue'
import pinia from './stores'
import App from './App.vue'
import router from './router'
import 'uno.css'

new Vue({
  pinia,
  router,
  render: (h) => h(App)
}).$mount('#app')
