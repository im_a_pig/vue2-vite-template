import { getCurrentInstance } from 'vue'

function useRouter() {
  const instance = getCurrentInstance()
  const router = instance.proxy.$router
  return router
}

function useRoute() {
  const instance = getCurrentInstance()
  const route = instance.proxy.$route
  return route
}

export { useRouter, useRoute }
