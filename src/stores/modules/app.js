import { defineStore } from 'pinia'

export const useStore = defineStore('app', {
  state: () => ({
    count: 0
  }),
  actions: {
    add() {
      this.count++
    }
  }
})
